const express = require('express');
const { Booking } = require('../db');

function list(req, res, next) {
    Booking.findAll().then(objects => res.json(objects))
    .catch();
  }

function index(req, res, next) {
  const id = req.params.id;
  Booking.findByPk(id).then(obj => res.json(obj)).catch(err => res.send(err));
}

function create(req, res, next) {
  const date = req.body.date;
  const memberId= req.body.member_id;
  const copyId = req.body.copy_id;

  let booking = new Object({
    date: date,
    memberId: memberId,
    copyId: copyId
  });

  Booking.create(booking)
  .then(obj => res.json(obj))
  .catch(error => res.send(err));
}

function replace(req, res, next) {
  const id = req.params.id;
  Booking.findByPk(id).then((object)=>{
    const date = req.body.date ? req.body.date: "";
    object.update({description:description})
          .then(booking => res.json(booking))
          .catch(err => res.send(err));
  }).catch(err => res.send(err));
}

function update(req, res, next) {
  const id = req.params.id;
  Booking.findByPk(id).then((object)=>{
    const date = req.body.date ? req.body.date: object.date;
    object.update({date:date})
          .then(booking => res.json(booking))
          .catch(err => res.send(err));
  }).catch(err => res.send(err));
}

function destroy(req, res, next) {
  const id = req.params.id;
  Booking.destroy({where: {id:id}})
                .then(obj => res.json(obj))
                .catch(err => res.send(err));
}

  module.exports = { list, index, create, replace, update, destroy };  