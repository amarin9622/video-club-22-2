const express = require('express');
const { Copy } = require('../db');

function list(req, res, next) {
    Copy.findAll({include:['movies', 'bookings']}).then(objects => res.json(objects))
    .catch();
  }

function index(req, res, next) {
  const id = req.params.id;
  Copy.findByPk(id).then(obj => res.json(obj)).catch(err => res.send(err));
}


function create(req, res, next) {
  const number = req.body.number;
  const movie_id = req.body.movie_id;
  const status = req.body.status;

  let copy = new Object({
    number: number,
    status:status,
    movie_id:movie_id
  });

  Copy.create(copy)
  .then(obj => res.json(obj))
  .catch(error => res.send(err));
}

function replace(req, res, next) {
  const id = req.params.id;
  Copy.findByPk(id).then((object)=>{
    const status = req.body.status ? req.body.status : false;
    object.update({status:status})
          .then(copy => res.json(copy))
          .catch(err => res.send(err));
  }).catch(err => res.send(err));
}

function update(req, res, next) {
  const id = req.params.id;
  Copy.findByPk(id).then((object)=>{
    const status = req.body.status ? req.body.status : object.status;
    object.update({status:status})
          .then(copy => res.json(copy))
          .catch(err => res.send(err));
  }).catch(err => res.send(err));
}

function destroy(req, res, next) {
  const id = req.params.id;
  Copy.destroy({where: {id:id}})
                .then(obj => res.json(obj))
                .catch(err => res.send(err));
}

  module.exports = { list, index, create, replace, update, destroy };  