const express = require('express');
const { Director } = require('../db');

function list(req, res, next) {
    Director.findAll({include:[]}).then(objects => res.json(objects))
    .catch();
  }

function index(req, res, next) {
  const id = req.params.id;
  Director.findByPk(id).then(obj => res.json(obj)).catch(err => res.send(err));
}


function create(req, res, next) {
  const name = req.body.name;
  const last_name = req.body.last_name;

  let director = new Object({
    name:name,
    last_name:last_name
  });

  Director.create(director)
  .then(obj => res.json(obj))
  .catch(error => res.send(err));
}

function replace(req, res, next) {
  const id = req.params.id;
  Director.findByPk(id).then((object)=>{
    const name = req.body.name ? req.body.name : "";
    const last_name = req.body.last_name ? req.body.last_name : "";
    object.update({name:name, last_name:last_name})
          .then(director => res.json(director))
          .catch(err => res.send(err));
  }).catch(err => res.send(err));
}

function update(req, res, next) {
  const id = req.params.id;
  Director.findByPk(id).then((object)=>{
    const name = req.body.name ? req.body.name : object.name;
    const last_name = req.body.last_name ? req.body.last_name : object.last_name;
    object.update({name:name, last_name:last_name})
          .then(director => res.json(director))
          .catch(err => res.send(err));
  }).catch(err => res.send(err));
}

function destroy(req, res, next) {
  const id = req.params.id;
  Director.destroy({where: {id:id}})
                .then(obj => res.json(obj))
                .catch(err => res.send(err));
}

  module.exports = { list, index, create, replace, update, destroy };  