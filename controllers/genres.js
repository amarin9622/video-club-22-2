const express = require('express');
const { Genre } = require('../db');
const genre = require('../models/genre');

function list(req, res, next) {
  Genre.findAll().then(objects => res.json(objects))
  .catch();
}

function index(req, res, next) {
  const id = req.params.id;
  Genre.findByPk(id).then(obj => res.json(obj)).catch(err => res.send(err));
}

function create(req, res, next) {
  const description = req.body.description;
  const status= req.body.status;

  let genre = new Object({
    description: description,
    status:status
  });

  Genre.create(genre)
  .then(obj => res.json(obj))
  .catch(err => res.send(err));
}

function replace(req, res, next) {
  const id = req.params.id;
  Genre.findByPk(id).then((object)=>{
    const description = req.body.description ? req.body.description: "";
    const status = req.body.status ? req.body.status : false;
    object.update({description:description, status:status})
          .then(genre => res.json(genre))
          .catch(err => res.send(err));
}).catch(err => res.send(err));
}

function update(req, res, next) {
  const id = req.params.id;
  Genre.findByPk(id).then((object)=>{
    const description = req.body.description ? req.body.description: object.description;
    const status = req.body.status ? req.body.status: object.status;
    object.update({description:description, status})
          .then(genre => res.json(genre))
          .catch(err => res.send(err));
  }).catch(err => res.send(err));
}

function destroy(req, res, next) {
  const id = req.params.id;
  genre.destroy({where: {id:id}})
                .then(obj => res.json(obj))
                .catch(err => res.send(err));
}

module.exports = { list, index, create, replace, update, destroy }