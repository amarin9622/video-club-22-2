const express = require('express');
const { Copy } = require('../db');

function list(req, res, next) {
    Copy.findAll({include:['directors']}).then(objects => res.json(objects))
    .catch();
  }

function index(req, res, next) {
  const id = req.params.id;
  Copy.findByPk(id).then(obj => res.json(obj)).catch(err => res.send(err));
}


function create(req, res, next) {
  const title = req.body.title;
  const genre_id = req.body.genre_id;
  const director_id = req.body.director_id;

  let movie = new Object({
    title: title,
    genre_id:genre_id,
    director_id:director_id
  });

  Movie.create(movie)
  .then(obj => res.json(obj))
  .catch(error => res.send(err));
}

function replace(req, res, next) {
  const id = req.params.id;
  Movie.findByPk(id).then((object)=>{
    const title = req.body.title ? req.body.title : "";
    object.update({title:title})
          .then(movie => res.json(movie))
          .catch(err => res.send(err));
  }).catch(err => res.send(err));
}

function update(req, res, next) {
  const id = req.params.id;
  Movie.findByPk(id).then((object)=>{
    const status = req.body.status ? req.body.status : object.status;
    object.update({status:status})
          .then(movie => res.json(movie))
          .catch(err => res.send(err));
  }).catch(err => res.send(err));
}

function destroy(req, res, next) {
  const id = req.params.id;
  Movie.destroy({where: {id:id}})
                .then(obj => res.json(obj))
                .catch(err => res.send(err));
}

  module.exports = { list, index, create, replace, update, destroy };  