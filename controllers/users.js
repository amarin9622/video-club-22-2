const express = require('express');

function list(req, res, next) {
    res.send('respond with a list');
  }

function index(req, res, next) {
  const id = req.params.id;
  res.send(`index Parametros => ${id}`);
}

function create(req, res, next) {
  const name = req.body.name;
  const lastName= req.body.lastName;
  res.send(`create => parametros ${name} ${lastName}`);
}

function replace(req, res, next) {
  res.send('respond with a replace');
}

function update(req, res, next) {
  res.send('respond with a update');
}

function destroy(req, res, next) {
  res.send('respond with a destroy');
}

  module.exports = { list, index, create, replace, update, destroy };  