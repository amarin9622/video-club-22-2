const Sequelize = require('sequelize');
const genreModel = require('./models/genre')
const movieModel = require('./models/movie');
const actorModel = require('./models/actor');
const directorModel = require('./models/director');
const bookingModel = require('./models/booking');
const copyModel = require('./models/copy');
const memberModel = require('./models/member');


// 1) Nombre de la base de datos.
// 2) Usuario de la base de datos.
// 3) Contraseña de la base de datos.
// 4) Objeto de configuración (ORM).

const sequelize = new Sequelize('video_club', 'root', 'secret', 
{
    host:'127.0.0.1',
    dialect:'mysql'
});

const Genre = genreModel(sequelize, Sequelize)
const Movie = movieModel(sequelize, Sequelize)
const Actor = actorModel(sequelize, Sequelize)
const Director = directorModel(sequelize, Sequelize)
const Booking = bookingModel(sequelize, Sequelize)
const Copy = copyModel(sequelize, Sequelize)
const Member = memberModel(sequelize, Sequelize)

Genre.hasMany(Movie, {as: 'movies'});

Movie.belongsTo(Genre, {as: 'genre'});

Director.hasMany(Movie, {as: 'movies'});

Movie.belongsTo(Director, {as: 'directors'});

Booking.hasMany(Member, {as: 'members'});

Member.belongsTo(Booking, {as: 'bookings'});

Movie.hasMany(Copy,{as:'copies'});

sequelize.sync({
    force:true
}).then(()=>{
    console.log("Base de datos actualizada")
});

module.exports = { Genre, Movie, Actor, Director, Booking, Copy, Member };