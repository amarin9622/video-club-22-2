module.exports =(sequelize, type) =>{
    const Director = sequelize.define('director', {
        id: {type: type.INTEGER, primaryKey:true, autoIncrement:true},
        description: type.STRING,
        status: type.BOOLEAN
    });
    return Director
};